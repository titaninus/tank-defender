﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public Transform TargetCamera;
    public Transform Tank;
    public float CameraPositionAngle = 45f;
    public float CameraDistance = 1f;
    public float CameraLookAngle = 45f;
    private float _cameraPolarAngle;

    private void OnValidate()
    {
        _cameraPolarAngle = CameraPositionAngle * Mathf.Deg2Rad;
    }

    private void Start()
    {
        _cameraPolarAngle = CameraPositionAngle * Mathf.Deg2Rad;
    }


    // Update is called once per frame
    void Update()
    {
        var azimuth = Tank.eulerAngles.y * Mathf.Deg2Rad;
        
        TargetCamera.forward = Tank.forward;
        TargetCamera.Rotate(new Vector3(1, 0, 0f), CameraLookAngle);
        TargetCamera.position = Tank.position + new Vector3(
            -CameraDistance * Mathf.Sin(_cameraPolarAngle) * Mathf.Sin(azimuth), 
            CameraDistance * Mathf.Cos(_cameraPolarAngle),
            -CameraDistance * Mathf.Sin(_cameraPolarAngle) * Mathf.Cos(azimuth));
    }
    
    
}
