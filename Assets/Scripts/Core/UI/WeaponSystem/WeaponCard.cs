﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Core.UI.WeaponSystem
{
    public class WeaponCard : MonoBehaviour
    {
        public Image Background;
        public RawImage PreviewImage;
        public TMP_Text WeaponName;
    }
}