﻿using System;
using Tank.Weapon;
using TMPro;
using UnityEngine;

namespace Core.UI.WeaponSystem
{
    public class WeaponMenuController : MonoBehaviour
    {
        public GameObject VisualPart;
        [Header("Reload Bar")]
        public RectTransform ReloadBarBackground;
        public RectTransform ReloadBarMask;
        public TMP_Text ReloadTimeText;
        private float _maxReloadBarWidth;

        [Header("Weapons")]
        public WeaponCard MainCard;
        public WeaponCard PrevCard;
        public WeaponCard NextCard;

        [Header("WeaponInfo")] 
        public TMP_Text DamageInfo;
        public TMP_Text SpeedInfo;
        public TMP_Text MassInfo;
        
        
        private TankWeaponStorage _storage;
        private ITankWeaponController _tankWeaponController;
        private GameManager _gameManager;

        private void Start()
        {
            VisualPart.SetActive(false);
            _gameManager = DependencyResolver.GetGameManager();
            _gameManager.OnGameStarted.AddListener(OnGameStartedHandler);
            _gameManager.OnGameFinished.AddListener(OnGameFinishedHandler);
            _tankWeaponController = DependencyResolver.GetTankWeaponController();
            _tankWeaponController.OnWeaponChanged.AddListener(UpdateWeaponOrder);
            _storage = _tankWeaponController.GetWeaponStorage();
            _maxReloadBarWidth = ReloadBarBackground.rect.width;
        }


        private void UpdateWeaponOrder()
        {
            var currentWeapon = _storage.GetCurrentWeapon();
            var prevWeapon = _storage.GetPrevWeaponSafe();
            var nextWeapon = _storage.GetNextWeaponSafe();

            MainCard.PreviewImage.texture = currentWeapon.GetWeaponImage();
            MainCard.WeaponName.text = currentWeapon.GetName();
            
            NextCard.PreviewImage.texture = prevWeapon.GetWeaponImage();
            NextCard.WeaponName.text = prevWeapon.GetName();
            
            PrevCard.PreviewImage.texture = nextWeapon.GetWeaponImage();
            PrevCard.WeaponName.text = nextWeapon.GetName();

            DamageInfo.text = $"{currentWeapon.GetCooldown():0.}";
            SpeedInfo.text = $"{currentWeapon.GetBulletSpeed():0.}";
            MassInfo.text = $"{currentWeapon.GetBulletMass():0.}";

        }

        private void OnGameFinishedHandler()
        {
            VisualPart.SetActive(false);
        }
        
        private void OnGameStartedHandler()
        {
            VisualPart.SetActive(true);
            
            UpdateWeaponOrder();
            UpdateCooldownBar();
        }

        private void Update()
        {
            if (_gameManager.IsGameRunning && !_gameManager.IsGamePaused)
            {
                UpdateCooldownBar();
            }
        }

        private void UpdateCooldownBar()
        {
            var cooldownRate = _tankWeaponController.GetCurrentCooldown() /  _storage.GetCurrentWeapon().GetCooldown();
            var maskWidth = (1 - cooldownRate) * _maxReloadBarWidth;
            ReloadBarMask.sizeDelta = new Vector2(maskWidth, ReloadBarMask.sizeDelta.y);
            ReloadTimeText.text = $"{_tankWeaponController.GetCurrentCooldown():0.0} s";
        }
    }
}