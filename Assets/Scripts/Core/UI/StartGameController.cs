﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Core.UI
{
    public class StartGameController : MonoBehaviour
    {
        public GameObject VisualPart;
        public Button StartGameButton;

        private void Awake()
        {
            StartGameButton.onClick.AddListener(OnStartGameButtonClicked);
        }

        private void OnStartGameButtonClicked()
        {
            VisualPart.SetActive(false);
            DependencyResolver.GetGameManager().RunGame();
        }
    }
}