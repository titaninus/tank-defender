﻿using System;
using Tank;
using TMPro;
using UnityEngine;

namespace Core.UI
{
    public class HitpointIndicatorController : MonoBehaviour
    {
        public GameObject VisualPart;
        [Header("HitPoint Bar")]
        public RectTransform HitPointBarBackground;
        public RectTransform HitPointBarMask;
        public TMP_Text MaxHitpointText;
        public TMP_Text CurrentHitpointText;
        private float _maxReloadBarWidth;
        private TankController _tankController;
        private GameManager _gameManager;

        private void Start()
        {
            _gameManager = DependencyResolver.GetGameManager();
            _gameManager.OnGameStarted.AddListener(OnGameStartedHandler);
            _gameManager.OnGameFinished.AddListener(OnGameFinishedHandler);
            _maxReloadBarWidth = HitPointBarBackground.rect.width;
            _tankController = DependencyResolver.GetTankController();
            MaxHitpointText.text = $"{_tankController.MaxTankHealth:0.}";
            VisualPart.SetActive(false);
        }

        
        private void OnGameFinishedHandler()
        {
            VisualPart.SetActive(false);
        }
        
        private void OnGameStartedHandler()
        {
            VisualPart.SetActive(true);
        }
        
        private void Update()
        {
            var hitpointRate = _tankController.GetCurrentHealth() /  _tankController.MaxTankHealth;
            var maskWidth = hitpointRate * _maxReloadBarWidth;
            HitPointBarMask.sizeDelta = new Vector2(maskWidth, HitPointBarMask.sizeDelta.y);
            CurrentHitpointText.text = $"{_tankController.GetCurrentHealth():0.}";
        }
    }
}