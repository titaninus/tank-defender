﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Core.UI
{
    public class EndGameController : MonoBehaviour
    {
        public GameObject VisualPart;

        private void Start()
        {
            DependencyResolver.GetGameManager().OnGameFinished.AddListener(OnEndGame);
            VisualPart.SetActive(false);
        }

        private void OnEndGame()
        {
            VisualPart.SetActive(true);
        }
    }
}