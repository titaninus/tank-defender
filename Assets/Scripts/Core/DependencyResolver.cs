﻿using Monster;
using Tank;
using Tank.Movement;
using Tank.Weapon;

namespace Core
{
    public static class DependencyResolver
    {
        private static ITankMovementController _tankMovementController;

        public static void RegisterTankMovementController(ITankMovementController controller)
        {
            _tankMovementController = controller;
        }

        public static ITankMovementController GetTankMovementController()
        {
            return _tankMovementController;
        }
        
        
        private static ITankWeaponController _tankWeaponController;

        public static void RegisterTankWeaponController(ITankWeaponController controller)
        {
            _tankWeaponController = controller;
        }

        public static ITankWeaponController GetTankWeaponController()
        {
            return _tankWeaponController;
        }

        private static GameManager _gameManager;
        
        public static void RegisterGameManager(GameManager gameManager)
        {
            _gameManager = gameManager;
        }

        public static GameManager GetGameManager()
        {
            return _gameManager;
        }
        
        private static MonsterManager _monsterManager;
        
        public static void RegisterMonsterManager(MonsterManager monsterManager)
        {
            _monsterManager = monsterManager;
        }

        public static MonsterManager GetMonsterManager()
        {
            return _monsterManager;
        }
        
        private static TankController _tankController;
        
        public static void RegisterTankController(TankController tankController)
        {
            _tankController = tankController;
        }

        public static TankController GetTankController()
        {
            return _tankController;
        }
    }
}