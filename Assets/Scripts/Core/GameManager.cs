﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Core
{
    public class GameManager : MonoBehaviour
    {
        // Logical state of game
        public bool IsGameRunning { get; private set; }
        // Physical state of game
        public bool IsGamePaused { get; private set; }
        public UnityEvent OnGameStarted = new UnityEvent();
        public UnityEvent OnGameFinished = new UnityEvent();
        public UnityEvent OnGamePaused = new UnityEvent();
        public UnityEvent OnGameUnpaused = new UnityEvent();

        private void Awake()
        {
            DependencyResolver.RegisterGameManager(this);
            IsGameRunning = false;
            IsGamePaused = true;
        }
        
        public void RunGame()
        {
            if (!IsGameRunning) {
                IsGameRunning = true;
                IsGamePaused = false;
                OnGameUnpaused.Invoke();
                OnGameStarted.Invoke();
            }
        }

        public void FinishGame()
        {
            if (IsGameRunning)
            {
                IsGameRunning = false;
                Pause();
                OnGameFinished.Invoke();
            }
        }

        public void Pause()
        {
            if (!IsGamePaused)
            {
                IsGamePaused = true;
                OnGamePaused.Invoke();
            }
        }

        public void Unpause()
        {
            if (IsGamePaused)
            {
                IsGamePaused = false;
                OnGameUnpaused.Invoke();
            }
        }
        
    }
}