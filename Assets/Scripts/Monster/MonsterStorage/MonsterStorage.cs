﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tank;
using Newtonsoft.Json;
using UnityEngine;
using Utilities;
using Random = System.Random;

namespace Monster
{
    [CreateAssetMenu(fileName = "MonsterStorage", menuName = "Tools/CreateMonsterStorage", order = 0)]
    public class MonsterStorage : ScriptableObject
    {

        [SerializeReference]
        public List<IMonster> Monsters;

        private Random _generator;


        public IMonster GetRandomMonster()
        {
            if (_generator == null)
            {
                _generator = new Random(DateTime.Now.Millisecond);
            }
            var normalizeFactor = GetNormalizeFactor();
            float randomNumber = Convert.ToSingle(_generator.NextDouble());
            var accumulator = 0f;
            foreach (var monster in Monsters)
            {
                var normalizedRate = monster.GetSpawnRate() / normalizeFactor;
                if (randomNumber > accumulator && (normalizedRate + accumulator >= randomNumber))
                {
                    return monster;
                }
                else
                {
                    accumulator += normalizedRate;
                }
            }

            return Monsters.Last();
        }

        private float GetNormalizeFactor()
        {
            return Monsters.Select(m => m.GetSpawnRate()).Sum();
        }
    }
}