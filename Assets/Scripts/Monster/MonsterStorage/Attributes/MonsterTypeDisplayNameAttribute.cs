﻿using System;

namespace Monster
{
    [AttributeUsage(AttributeTargets.Class)]
    public class MonsterTypeDisplayNameAttribute : System.Attribute
    {
        public readonly string DisplayName;
        public MonsterTypeDisplayNameAttribute(string displayName)
        {
            DisplayName = displayName;
        }
    }
}