﻿using System;

namespace Monster
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class ExcludeMonsterBehaviourTypeFromStorageAttribute : System.Attribute
    {
        public ExcludeMonsterBehaviourTypeFromStorageAttribute()
        {
        }
    }
}