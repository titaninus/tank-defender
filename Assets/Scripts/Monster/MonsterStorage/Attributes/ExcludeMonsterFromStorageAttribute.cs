﻿using System;

namespace Monster
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class ExcludeMonsterFromStorageAttribute : System.Attribute
    {
        public ExcludeMonsterFromStorageAttribute()
        {
        }
    }
}