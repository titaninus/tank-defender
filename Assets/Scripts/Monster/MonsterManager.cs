﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using Unity.Collections;
using UnityEngine;
using Random = System.Random;

namespace Monster
{
    public class MonsterManager : MonoBehaviour
    {
        public MonsterStorage Storage;
        public int MonsterAmount = 10;
        private Random _generator;

        [SerializeField, ReadOnly]
        private readonly List<MonsterSpawnPoint> SpawnPoints = new List<MonsterSpawnPoint>();
        
        private void Awake()
        {
            DependencyResolver.RegisterMonsterManager(this);
            if (_generator == null)
            {
                _generator = new Random(DateTime.Now.Millisecond);
            }
        }

        private void Start()
        {
            MonsterPool.CreatePool(MonsterAmount, Storage.Monsters, new GameObject("Monster Pool"));
            DependencyResolver.GetGameManager().OnGameStarted.AddListener(StartMonsterInstantiate);
        }

        public void RegisterSpawnPoint(MonsterSpawnPoint target)
        {
            SpawnPoints.Add(target);
        }
        
        private void StartMonsterInstantiate()
        {
            for (var i = 0; i < MonsterAmount; ++i)
            {
                AwakeRandomMonster();
            }
        }

        private void AwakeRandomMonster()
        {
            var controller = MonsterPool.Instance.GetMonsterInstance(Storage.GetRandomMonster())
                .GetComponent<MonsterController>();
            var spawnPoint = GetRandomSpawnPoint();
            controller.SetPosition(spawnPoint.position);
            controller.MonsterBehaviour.Enable();
            controller.Run();
            controller.OnMonsterDie.AddListener(() => SetMonsterDie(controller));
        }

        private void SetMonsterDie(MonsterController monsterController)
        {
            monsterController.MonsterBehaviour.Disable();
            AwakeRandomMonster();
        }

        public Transform GetRandomSpawnPoint()
        {
            var normalizeFactor = GetNormalizeFactor();
            float randomNumber = Convert.ToSingle(_generator.NextDouble());
            var accumulator = 0f;
            foreach (var spawnPoint in SpawnPoints)
            {
                var normalizedRate = spawnPoint.SpawnRate / normalizeFactor;
                if (randomNumber > accumulator && (normalizedRate + accumulator >= randomNumber))
                {
                    return spawnPoint.transform;
                }
                else
                {
                    accumulator += normalizedRate;
                }
            }

            return SpawnPoints.Last().transform;
        }

        private float GetNormalizeFactor()
        {
            return SpawnPoints.Select(sp => sp.SpawnRate).Sum();
        }
    }
}