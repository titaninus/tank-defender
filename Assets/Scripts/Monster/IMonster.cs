﻿using System;
using UnityEngine;

namespace Monster
{
    public interface IMonster
    {
        float GetSpawnRate();
        GameObject GetMonsterPrefab();
        Type GetMonsterBehaviour();
        string GetName();
        float GetMaxHealth();
        float GetDefense();
        float GetDamage();

        float GetSpeed();
    }
}