﻿using UnityEngine;
using UnityEngine.Events;
using Utilities.Mono;

namespace Monster.MonsterBehaviour
{
    [ExcludeMonsterBehaviourTypeFromStorage]
    public abstract class MonsterBehaviourBase
    {
        public FloatEvent OnDamageCause = new FloatEvent();
        
        public abstract void Init(IMonster description, GameObject targetObject);
        public abstract void Run();

        public abstract float GetCurrentHealth();
        public abstract void TakeDamage(float damageValue);

        public abstract void SetPositionExternal(Vector3 spawnPointPosition);

        public abstract void Disable();
        public abstract void Enable();
        public abstract void Die();
    }
}