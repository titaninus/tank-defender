﻿using System;

namespace Monster.MonsterBehaviour
{
    [Serializable]
    public class MonsterBehaviourType
    {
        [NonSerialized]
        private Type _monsterBehaviour;
        public string QualifiedMonsterBehaviourType;

        public Type GetMonsterType()
        {
            if (_monsterBehaviour == null)
            {
                _monsterBehaviour = Type.GetType(QualifiedMonsterBehaviourType);
            }
            return _monsterBehaviour;
        }
    }
}