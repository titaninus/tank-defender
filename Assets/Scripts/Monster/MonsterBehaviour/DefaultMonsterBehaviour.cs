﻿using System;
using System.Collections;
using Core;
using UnityEngine;
using UnityEngine.AI;

namespace Monster.MonsterBehaviour
{
    [Serializable]
    public class DefaultMonsterBehaviour : MonsterBehaviourBase
    {
        [SerializeReference]
        private IMonster _description;
        private GameObject _monsterObject;
        [SerializeField]
        private float _currentHealth;
        private NavMeshAgent _agent;

        private Coroutine _updateRoutine;
        private DefaultMonsterCollisionChecker _collisionChecker;
        [SerializeField]
        private bool _isEnabled;


        public override void Init(IMonster description, GameObject targetObject)
        {
            _description = description;
            _monsterObject = targetObject;
            _currentHealth = _description.GetMaxHealth();
            var gameManager = DependencyResolver.GetGameManager();
            gameManager.OnGameFinished.AddListener(OnGameFinished);
            gameManager.OnGamePaused.AddListener(OnGamePaused);
            gameManager.OnGameUnpaused.AddListener(OnGameUnpaused);
            
            _collisionChecker = _monsterObject.AddComponent<DefaultMonsterCollisionChecker>();
            _collisionChecker.OnCollisionHappend.AddListener(CauseDamage);
            
            var physicsBody = _monsterObject.AddComponent<Rigidbody>();
            physicsBody.isKinematic = true;
            _agent = _monsterObject.AddComponent<NavMeshAgent>();
            _agent.speed = _description.GetSpeed();
            //_agent.isStopped = true;
        }

        public override void Run()
        {
            _currentHealth = _description.GetMaxHealth();
            Enable();
            if (_updateRoutine == null)
            {
                _updateRoutine = _collisionChecker.StartCoroutine(UpdateRoutine());
                if (!DependencyResolver.GetGameManager().IsGamePaused)
                {
                    OnGameUnpaused();
                }
            }
        }

        public void OnGameFinished()
        {
            StopUpdateRoutine();
        }

        private void OnGamePaused()
        {
            if (_isEnabled)
            {
                _agent.isStopped = true;
            }
        }

        private void OnGameUnpaused()
        {
            if (_isEnabled)
            {
                _agent.isStopped = false;
            }
        }

        private IEnumerator UpdateRoutine()
        {
            while (DependencyResolver.GetGameManager().IsGameRunning)
            {
                if (!DependencyResolver.GetGameManager().IsGamePaused)
                {
                    _agent.destination = DependencyResolver.GetTankController().GetTankPosition();
                }

                yield return null;
            }
        }

        public override float GetCurrentHealth()
        {
            return _currentHealth;
        }

        public override void TakeDamage(float damageValue)
        {
            _currentHealth -= damageValue * (1 - _description.GetDefense());
        }

        public override void SetPositionExternal(Vector3 spawnPointPosition)
        {
            _monsterObject.transform.position = spawnPointPosition;
        }

        public override void Disable()
        {
            StopUpdateRoutine();
            _monsterObject.SetActive(false);
            _isEnabled = false;
        }

        public override void Enable()
        {
            _monsterObject.SetActive(true);
            _isEnabled = true;
        }

        public override void Die()
        {
            if (_isEnabled)
            {
                Disable();
                _currentHealth = _description.GetMaxHealth();
            }

            StopUpdateRoutine();
        }

        private void StopUpdateRoutine()
        {
            if (_updateRoutine != null)
            {
                _collisionChecker.StopCoroutine(_updateRoutine);
                _updateRoutine = null;
            }
        }

        private void CauseDamage()
        {
            OnDamageCause.Invoke(_description.GetDamage());
        }
    }
}