﻿using System;
using Tank;
using UnityEngine;
using UnityEngine.Events;

namespace Monster.MonsterBehaviour
{
    public class DefaultMonsterCollisionChecker : MonoBehaviour
    {
        public UnityEvent OnCollisionHappend = new UnityEvent();
        
        private void OnCollisionStay(Collision other)
        {
            var controller = other.gameObject.GetComponent<TankController>();
            if (controller != null)
            {
                OnCollisionHappend.Invoke();
            }
        }
    }
}