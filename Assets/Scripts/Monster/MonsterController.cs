﻿using System;
using Core;
using Monster.MonsterBehaviour;
using Tank;
using UnityEngine;
using UnityEngine.Events;

namespace Monster
{
    [Serializable]
    public class MonsterController : MonoBehaviour
    {
        [SerializeReference]
        public MonsterBehaviourBase MonsterBehaviour;
        [SerializeReference]
        public IMonster Description;

        public UnityEvent OnMonsterDie = new UnityEvent();

        public void Init(IMonster description)
        {
            Description = description;
            gameObject.tag = "Monster";
            MonsterBehaviour.Init(Description, gameObject);
            MonsterBehaviour.OnDamageCause.AddListener(OnCauseDamage);
        }
        
        public void Run()
        {
            MonsterBehaviour.Run();
        }

        public void TakeDamage(float damageValue)
        {
            MonsterBehaviour.TakeDamage(damageValue);
            if (MonsterBehaviour.GetCurrentHealth() <= 0)
            {
                MonsterBehaviour.Die();
                OnMonsterDie.Invoke();
            }
        }

        public void OnCauseDamage(float damageValue)
        {
            DependencyResolver.GetTankController().TakeDamage(damageValue);
        }

        public void SetPosition(Vector3 spawnPointPosition)
        {
            MonsterBehaviour.SetPositionExternal(spawnPointPosition);
        }
    }
}