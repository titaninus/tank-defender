﻿using System;
using Core;
using UnityEngine;

namespace Monster
{
    public class MonsterSpawnPoint : MonoBehaviour
    {

        public float SpawnRate = 0.1f;
        private void Start()
        {
            DependencyResolver.GetMonsterManager().RegisterSpawnPoint(this);
        }

        private void OnDrawGizmosSelected()
        {
            var color = Gizmos.color;
            Gizmos.color = Color.blue;
            Gizmos.DrawCube(transform.position, Vector3.one);
            Gizmos.color = color;
        }
    }
}