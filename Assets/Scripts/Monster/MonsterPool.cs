﻿using System.Collections.Generic;
using System.Linq;
using Monster.MonsterBehaviour;
using UnityEngine;

namespace Monster
{
    public class MonsterPool
    {
        public class MonsterInstance
        {
            public GameObject Instance;
            public IMonster Description;
            public bool IsAlive;
        }

        private readonly List<IMonster> _registeredMonsters;
        private readonly Dictionary<IMonster, List<MonsterInstance>> _pool;
        private readonly Dictionary<IMonster, GameObject> _rootMap;
        
        private static MonsterPool _instance;
        private GameObject _poolParent;
        public static MonsterPool Instance => _instance ?? CreatePool(0, new List<IMonster>(), new GameObject("MonsterPool"));

        public static MonsterPool CreatePool(int defaultObjectsAmount, IEnumerable<IMonster> monsters, GameObject poolParent)
        {
            _instance?.FreePool();
            _instance = new MonsterPool(poolParent);
            
            foreach (var monsterDescription in monsters)
            {
                _instance.PreparePoolForMonsterWithType(monsterDescription);
                for (var i = 0; i < defaultObjectsAmount; ++i)
                {
                    _instance.CreateMonsterInstance(monsterDescription);
                }
            }

            return _instance;
        }


        private MonsterPool(GameObject poolParent)
        {
            _registeredMonsters = new List<IMonster>();
            _pool = new Dictionary<IMonster, List<MonsterInstance>>();
            _rootMap = new Dictionary<IMonster, GameObject>();
            _poolParent = poolParent;
        }

        ~MonsterPool()
        {    
            FreePool();
        }
        
        private void PreparePoolForMonsterWithType(IMonster description)
        {
            if (_pool.ContainsKey(description))
            {
                return;
            }
            var monsterRoot = new GameObject($"{description.GetName()} pool");
            monsterRoot.transform.SetParent(_poolParent.transform);
            monsterRoot.transform.position = _poolParent.transform.position;
            _rootMap.Add(description, monsterRoot);
            _registeredMonsters.Add(description);
            _pool.Add(description, new List<MonsterInstance>());
        }
        
        private void CreateMonsterInstance(IMonster monsterDescription)
        {
            if (!_pool.ContainsKey(monsterDescription))
            {
                PreparePoolForMonsterWithType(monsterDescription);
            }
            var prefabInstance = GameObject.Instantiate(monsterDescription.GetMonsterPrefab(),
                _rootMap[monsterDescription].transform);
            prefabInstance.SetActive(false);
            var monsterInstance = new MonsterInstance()
            {
                Instance = prefabInstance,
                Description = monsterDescription,
                IsAlive = false
            };
            var controller = prefabInstance.AddComponent<MonsterController>();
            controller.OnMonsterDie.AddListener(() => SetMonsterNotAlive(monsterInstance));
            controller.MonsterBehaviour = System.Activator.CreateInstance(monsterDescription.GetMonsterBehaviour()) as MonsterBehaviourBase;
            controller.Init(monsterDescription);
            _pool[monsterDescription].Add(monsterInstance);
        }
        
        private void SetMonsterNotAlive(MonsterInstance instance)
        {
            instance.IsAlive = false;
            instance.Instance.SetActive(false);
        }

        
        private void FreePool()
        {
            foreach (var pair in _pool)
            {
                foreach (var monsterInstance in pair.Value)
                {
                    Object.Destroy(monsterInstance.Instance);
                }
            }
            _pool.Clear();
            if (_poolParent != null)
            {
                Object.Destroy(_poolParent);
            }
        }

        public GameObject GetMonsterInstance(IMonster description)
        {
            if (!_pool.ContainsKey(description))
            {
                PreparePoolForMonsterWithType(description);
            }

            if (_pool[description].All(mi => mi.IsAlive))
            {
                CreateMonsterInstance(description);
            }
            var monster = _pool[description].First(mi => !mi.IsAlive);
            monster.IsAlive = true;

            return monster.Instance;
        }
    }
}