﻿using System;
using Monster.MonsterBehaviour;
using Tank;
using UnityEngine;

namespace Monster
{
    [Serializable]
    [MonsterTypeDisplayName("Monster With Inspector")]
    public class MonsterWithCustomInspector : IMonster
    {
        public float SpawnRate;
        public GameObject MonsterPrefab;
        public MonsterBehaviourType MonsterBehaviour;
        public float MaxHealth;
        public float Defense;
        public float Damage;
        public string Name;
        public float Speed;

        public float GetSpawnRate()
        {
            return SpawnRate;
        }

        public GameObject GetMonsterPrefab()
        {
            return MonsterPrefab;
        }

        public Type GetMonsterBehaviour()
        {
            return MonsterBehaviour.GetMonsterType();
        }

        public string GetName()
        {
            return Name;
        }

        public float GetMaxHealth()
        {
            return MaxHealth;
        }

        public float GetDefense()
        {
            return Defense;
        }

        public float GetDamage()
        {
            return Damage;
        }

        public float GetSpeed()
        {
            return Speed;
        }
    }
}