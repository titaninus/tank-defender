﻿using System;
using Core;
using UnityEngine;

namespace Tank
{
    public class TankController : MonoBehaviour
    {

        public float MaxTankHealth;
        [Range(0, 1)]
        public float TankDefense;

        [SerializeField]
        private float _currentHealth;
        
        private void Awake()
        {
            DependencyResolver.RegisterTankController(this);
            _currentHealth = MaxTankHealth;
        }

        public void TakeDamage(float damageValue)
        {
            _currentHealth -= damageValue * (1 - TankDefense);
            if (_currentHealth <= 0)
            {
                DependencyResolver.GetGameManager().FinishGame();
            }
        }

        public Vector3 GetTankPosition()
        {
            return DependencyResolver.GetTankMovementController().GetTankPosition();
        }

        public float GetCurrentHealth()
        {
            return _currentHealth;
        }
    }
}