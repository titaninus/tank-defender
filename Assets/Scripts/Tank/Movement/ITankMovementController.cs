﻿using UnityEngine;

namespace Tank.Movement
{
    public interface ITankMovementController
    {
        void Move(bool forward);

        void Rotate(bool left);

        Vector3 GetTankPosition();
    }
}