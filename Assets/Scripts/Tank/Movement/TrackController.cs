﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tank.Movement
{


    public class TrackController : MonoBehaviour
    {

        private const float MOVE_THRESHOLD = 0.001f;

        public SkinnedMeshRenderer TrackRenderer;
        public float SpeedToOffsetMultiplier = 1f;

        private Material _trackInstancedMaterial;
        private bool _isInitialized;
        private float _cachedMove;


        private void Init()
        {
            if (_isInitialized)
            {
                return;
            }

            if (TrackRenderer == null)
            {
                return;
            }

            _trackInstancedMaterial = new Material(TrackRenderer.material);
            TrackRenderer.material = _trackInstancedMaterial;

            _isInitialized = true;
        }

        void Start()
        {
            Init();
        }

        public void Move(float distance)
        {
            _cachedMove += distance * SpeedToOffsetMultiplier;
        }

        // Update is called once per frame
        void Update()
        {
            if (!_isInitialized)
            {
                return;
            }

            if (Math.Abs(_cachedMove) > MOVE_THRESHOLD)
            {
                _trackInstancedMaterial.mainTextureOffset += new Vector2(_cachedMove, 0f);
                _cachedMove = 0f;
            }
        }

        private void OnDestroy()
        {
            Destroy(_trackInstancedMaterial);
        }
    }
}