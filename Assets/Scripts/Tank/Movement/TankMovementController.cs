﻿using System;
using Core;
using UnityEngine;

namespace Tank.Movement
{
    public class TankMovementController : MonoBehaviour, ITankMovementController
    {
        public TrackController LeftTrack;
        public TrackController RightTrack;

        public float RotateSpeed;
        public float MoveSpeed;
        public float TrackRotateSpeed;
        

        protected Transform _transform;
        protected Vector3 _rotateAxis;

        private void Awake()
        {
            DependencyResolver.RegisterTankMovementController(this);
            _transform = transform;
            _rotateAxis = new Vector3(0, 1f, 0);
        }

        public virtual void Move(bool forward)
        {
            var moveDistance = MoveSpeed * Time.deltaTime * (forward? 1f : -1f);
            var direction = _transform.forward;
            _transform.Translate(moveDistance * direction, Space.World);
            LeftTrack.Move(moveDistance);
            RightTrack.Move(moveDistance);

        }

        public virtual void Rotate(bool left)
        {
            var rotateAngle = RotateSpeed * Time.deltaTime * (left ? -1f : 1f);
            _transform.Rotate(_rotateAxis, rotateAngle);
            if (left)
            {
                LeftTrack.Move(-TrackRotateSpeed);
                RightTrack.Move(TrackRotateSpeed);
            }
            else
            {
                LeftTrack.Move(TrackRotateSpeed);
                RightTrack.Move(-TrackRotateSpeed);
            }
        }

        public Vector3 GetTankPosition()
        {
            return _transform.position;
        }
    }
}