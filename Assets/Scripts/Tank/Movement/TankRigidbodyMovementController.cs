﻿using System;
using Core;
using UnityEngine;

namespace Tank.Movement
{
    [RequireComponent(typeof(Rigidbody))]
    public class TankRigidbodyMovementController : TankMovementController
    {
        protected const float VELOCITY_THRESHOLD = 0.01f;

        protected Rigidbody _physicsBody;
        protected bool _isInitialized;

        private void Init()
        {
            if (!_isInitialized)
            {
                _physicsBody = GetComponent<Rigidbody>();
                _physicsBody.isKinematic = false;
                _isInitialized = true;
                _transform = transform;
                _rotateAxis = new Vector3(0, 1f, 0);
            }
        }
        
        protected void Awake()
        {
            DependencyResolver.RegisterTankMovementController(this);
            Init();
        }

        public override void Move(bool forward)
        {
            var direction = _transform.forward;
            //_transform.Translate(moveDistance * direction, Space.World);
            _physicsBody.velocity = (direction * (MoveSpeed * (forward? 1f : -1f)));

        }

        protected void Update()
        {
            if (_physicsBody.velocity.magnitude > VELOCITY_THRESHOLD)
            {
                var isForwardMovement =
                    Mathf.Abs(Vector3.SignedAngle(_physicsBody.velocity, _transform.forward, _transform.up)) <= 90;
                var moveDistance = (_physicsBody.velocity * Time.deltaTime).magnitude;
                LeftTrack.Move(moveDistance * (isForwardMovement? 1f : -1f) );
                RightTrack.Move(moveDistance* (isForwardMovement? 1f : -1f));
            }
        }

        public override void Rotate(bool left)
        {
            var rotateAngle = RotateSpeed * Time.deltaTime * (left ? -1f : 1f);
            _physicsBody.angularVelocity = _rotateAxis * (RotateSpeed * (left ? -1f : 1f));
            if (left)
            {
                LeftTrack.Move(-TrackRotateSpeed);
                RightTrack.Move(TrackRotateSpeed);
            }
            else
            {
                LeftTrack.Move(TrackRotateSpeed);
                RightTrack.Move(-TrackRotateSpeed);
            }
        }
    }
}