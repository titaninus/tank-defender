﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using UnityEngine;
using UnityEngine.Events;

namespace Tank.Weapon
{
    public class TankWeaponController : MonoBehaviour, ITankWeaponController
    {
        private class WeaponInstance
        {
            public GameObject Instance;
            public Transform InstanceTransform;
            public float CurrentCooldown;
            public IWeapon WeaponDescription;
        }

        public TankWeaponStorage WeaponStorage;
        public Transform WeaponParent;
        public Transform TankObject;

        public UnityEvent OnSuccessfullShot;
        public UnityEvent OnShotAtCooldown;
        public UnityEvent OnWeaponChanged { get;  } = new UnityEvent();
        public TankWeaponStorage GetWeaponStorage()
        {
            return WeaponStorage;
        }

        public float GetCurrentCooldown()
        {
            return _instancedWeapon.CurrentCooldown;
        }

        private IWeapon _currentWeapon;
        private WeaponInstance _instancedWeapon;
        private Dictionary<IWeapon, WeaponInstance> WeaponToInstanceMapping;
        private GameManager _gameManager;

        private List<BulletPool.BulletDescription> GetAvailableBulletTypes()
        {
            var result = new List<BulletPool.BulletDescription>();

            foreach (var weapon in WeaponStorage.Weapons)
            {
                var description = new BulletPool.BulletDescription()
                {
                    BulletName = weapon.GetName(),
                    BulletPrefab = weapon.GetBulletPrefab(),
                    BulletSpeed = weapon.GetBulletSpeed(),
                    BulletMass = weapon.GetBulletMass(),
                    BulletDamage = weapon.GetDamage()
                };
                result.Add(description);
            }

            return result;
        }

        private WeaponInstance InstantiateWeapon(IWeapon target)
        {
            var weaponObject = Instantiate(target.GetWeaponPrefab(), WeaponParent);
            weaponObject.SetActive(false);
            var weaponInstance = new WeaponInstance()
            {
                Instance = weaponObject,
                InstanceTransform = weaponObject.transform,
                CurrentCooldown = 0,
                WeaponDescription = target
            };
            return weaponInstance;
        }

        private void InitWeapons()
        {
            WeaponToInstanceMapping = new Dictionary<IWeapon, WeaponInstance>();
            foreach (var weapon in WeaponStorage.Weapons)
            {
                var instance = InstantiateWeapon(weapon);
                WeaponToInstanceMapping.Add(weapon, instance);
            }
        }

        private void Awake()
        {
            DependencyResolver.RegisterTankWeaponController(this);
        }

        private void Start()
        {
            _gameManager = DependencyResolver.GetGameManager();
            InitWeapons();
            EnableWeapon(WeaponStorage.GetDefaultWeapon());
            BulletPool.CreatePool(4, GetAvailableBulletTypes(), WeaponParent.gameObject);
        }

        public void EnableWeapon(IWeapon target)
        {
            if (target == _currentWeapon)
            {
                return;
            }

            if (!WeaponToInstanceMapping.ContainsKey(target))
            {
                throw new InvalidOperationException(
                    $"Couldn't enable weapon {target.GetName()} because this weapon wasn't set up in controller");
            }

            _instancedWeapon?.Instance.SetActive(false);
            _currentWeapon = target;
            _instancedWeapon = WeaponToInstanceMapping[target];
            _instancedWeapon.Instance.SetActive(true);
            OnWeaponChanged.Invoke();
        }

        public void PickNextWeapon()
        {
            EnableWeapon(WeaponStorage.GetNextWeapon());
        }

        public void PickPrevWeapon()
        {
            EnableWeapon(WeaponStorage.GetPrevWeapon());
        }

        public void MakeShot()
        {
            if (_instancedWeapon == null)
            {
                throw new InvalidOperationException($"Couldn't shot because doesn't exists enabled weapon");
            }

            if (_instancedWeapon.CurrentCooldown <= 0)
            {
                MakeShotInternal();
            }
            else
            {
                OnShotAtCooldown.Invoke();
            }
        }

        public List<IWeapon> GetWeapons()
        {
            // Break the link with WeaponStorage weapons list
            return WeaponStorage.Weapons.ToList();
        }

        public IWeapon GetCurrentWeapon()
        {
            return _currentWeapon;
        }

        private void MakeShotInternal()
        {
            var bullet = BulletPool.Instance.GetBullet(_instancedWeapon.WeaponDescription.GetName());
            bullet.BulletRoot.transform.localPosition =_instancedWeapon.WeaponDescription.GetBulletStartPoint();
            bullet.BulletRoot.transform.localRotation = Quaternion.identity;
            bullet.gameObject.SetActive(true);
            bullet.Fire();
            _instancedWeapon.CurrentCooldown = _instancedWeapon.WeaponDescription.GetCooldown();

            OnSuccessfullShot.Invoke();
        }

        private void Update()
        {
            if (_instancedWeapon.CurrentCooldown > 0 && _gameManager.IsGameRunning && !_gameManager.IsGamePaused)
            {
                _instancedWeapon.CurrentCooldown -= Time.deltaTime;
            }
        }
    }
}