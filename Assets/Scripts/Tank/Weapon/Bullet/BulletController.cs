﻿using System;
using Monster;
using UnityEngine;
using UnityEngine.Events;

namespace Tank.Weapon
{
    public class BulletController : MonoBehaviour
    {
        public UnityEvent OnBulletImpact = new UnityEvent();
        public GameObject BulletRoot;
        public float BulletSpeed;
        public float BulletMass;
        public float BulletDamage;
        
        private bool _isInitialized;
        private Rigidbody physicsBody;

        private void Awake()
        {
            Init();
        }

        public void Init()
        {
            if (!_isInitialized)
            {
                physicsBody = GetComponent<Rigidbody>();
                if (physicsBody == null)
                {
                    physicsBody = gameObject.AddComponent<Rigidbody>();
                }

                physicsBody.isKinematic = false;
                physicsBody.mass = BulletMass;
                _isInitialized = true;
            }
        }
        
        public void Fire()
        {
            physicsBody.velocity = Vector3.zero;
            physicsBody.angularVelocity = Vector3.zero;
            physicsBody.AddForce(BulletRoot.transform.forward * BulletSpeed, ForceMode.Impulse);
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("DeadZone"))
            {
                OnBulletImpact.Invoke();
            }

            if (other.gameObject.CompareTag("Monster"))
            {
                var monsterController = other.gameObject.GetComponent<MonsterController>();
                monsterController.TakeDamage(BulletDamage);
                OnBulletImpact.Invoke();
            }
        }
    }
}