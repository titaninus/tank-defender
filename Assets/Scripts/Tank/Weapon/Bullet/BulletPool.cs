﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.TextCore;
using Object = UnityEngine.Object;

namespace Tank.Weapon
{
    public class BulletPool
    {

        private class BulletInstance
        {
            public GameObject Bullet;
            public bool IsAlive;
            public BulletDescription Description;
            public BulletController Controller;
        }
        
        public class BulletDescription
        {
            public GameObject BulletPrefab;
            public string BulletName;
            internal Transform BulletParent;
            public float BulletSpeed;
            public float BulletMass;
            public float BulletDamage;
        }
        
        private static BulletPool _instance;
        public static BulletPool Instance => _instance ?? CreatePool(0, new List<BulletDescription>(), new GameObject("BulletPool"));

        public static BulletPool CreatePool(int defaultObjectsAmount, IEnumerable<BulletDescription> bulletPrefabs, GameObject poolParent)
        {
            _instance?.FreePool();
            _instance = new BulletPool(poolParent);
            
            foreach (var bulletDescription in bulletPrefabs)
            {
                var bulletRoot = new GameObject($"{bulletDescription.BulletName} pool");
                bulletRoot.transform.SetParent(poolParent.transform);
                bulletRoot.transform.position = poolParent.transform.position;
                bulletDescription.BulletParent = bulletRoot.transform;
                _instance._descriptions.Add(bulletDescription.BulletName, bulletDescription);
                _instance._pool.Add(bulletDescription.BulletName, new List<BulletInstance>());
                
                for (var i = 0; i < defaultObjectsAmount; ++i)
                {
                    _instance.CreateBulletInstance(bulletDescription);
                }
            }

            return _instance;
        }
        
        private readonly Dictionary<string, List<BulletInstance>> _pool;
        private readonly Dictionary<string, BulletDescription> _descriptions;
        private readonly GameObject _poolParent;

        private BulletPool(GameObject poolParent)
        {
            _pool = new Dictionary<string, List<BulletInstance>>();
            _descriptions = new Dictionary<string, BulletDescription>();
            _poolParent = poolParent;
        }

        ~BulletPool()
        {    
            FreePool();
        }

        
        private void FreePool()
        {
            foreach (var pair in _pool)
            {
                foreach (var bulletInstance in pair.Value)
                {
                    Object.Destroy(bulletInstance.Bullet);
                }
            }
            _pool.Clear();
            if (_poolParent != null)
            {
                Object.Destroy(_poolParent);
            }
        }
        

        private void CreateBulletInstance(BulletDescription description)
        {
            var bulletObject = Object.Instantiate(description.BulletPrefab, description.BulletParent, true);
            bulletObject.SetActive(false);
            bulletObject.tag = "Bullet";
            var bulletController = bulletObject.AddComponent<BulletController>();
            bulletController.BulletRoot = bulletObject;
            bulletController.BulletMass = description.BulletMass;
            bulletController.BulletSpeed = description.BulletSpeed;
            bulletController.BulletDamage = description.BulletDamage;
            bulletController.Init();
            var bulletInstance = new BulletInstance()
            {
                Bullet = bulletObject,
                Description = description,
                IsAlive = false,
                Controller = bulletController
            };
            bulletController.OnBulletImpact.AddListener(() => SetBulletNotAlive(bulletInstance));
            _pool[description.BulletName].Add(bulletInstance);
        }

        private void SetBulletNotAlive(BulletInstance instance)
        {
            instance.IsAlive = false;
            instance.Bullet.SetActive(false);
        }

        public BulletController GetBullet(string bulletType)
        {
            if (!_descriptions.ContainsKey(bulletType))
            {
                throw new InvalidOperationException($"Couldn't get bullet with type {bulletType} because this type of bullet doesn't exists in Pool");
            }
            if (_pool[bulletType].TrueForAll(i => i.IsAlive))
            {
                CreateBulletInstance(_descriptions[bulletType]);
            }

            var bullet = _pool[bulletType].First(i => !i.IsAlive);
            bullet.IsAlive = true;
            return bullet.Controller;
        }
        
        
    }
}