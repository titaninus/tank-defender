﻿using System;
using UnityEngine;

namespace Tank.Weapon
{
    [Serializable]
    [WeaponTypeDisplayName("Default Weapon")]
    public class TankWeapon : IWeapon
    {
        public GameObject WeaponPrefab;
        public GameObject BulletPrefab;
        public Vector3 BulletStartPoint;
        public float BulletSpeed;
        public float BulletMass;
        public float Damage;
        public float Cooldown;
        public string Name;
        public Texture2D WeaponImage;

        public GameObject GetWeaponPrefab()
        {
            return WeaponPrefab;
        }

        public GameObject GetBulletPrefab()
        {
            return BulletPrefab;
        }

        public Vector3 GetBulletStartPoint()
        {
            return BulletStartPoint;
        }
        
        public float GetBulletSpeed()
        {
            return BulletSpeed;
        }
        
        public float GetBulletMass()
        {
            return BulletMass;
        }

        public float GetDamage()
        {
            return Damage;
        }

        public float GetCooldown()
        {
            return Cooldown;
        }

        public string GetName()
        {
            return Name;
        }

        public Texture2D GetWeaponImage()
        {
            return WeaponImage;
        }

        public void SetWeaponImage(Texture2D image)
        {
            WeaponImage = image;
        }
    }
}