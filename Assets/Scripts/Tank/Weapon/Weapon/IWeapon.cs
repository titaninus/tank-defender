﻿using System;
using UnityEngine;

namespace Tank.Weapon
{
    public interface IWeapon
    {
        GameObject GetWeaponPrefab();
        GameObject GetBulletPrefab();
        Vector3 GetBulletStartPoint();
        float GetBulletSpeed();
        float GetBulletMass();
        float GetDamage();
        float GetCooldown();
        string GetName();
        Texture2D GetWeaponImage();
        void SetWeaponImage(Texture2D image);
    }
}