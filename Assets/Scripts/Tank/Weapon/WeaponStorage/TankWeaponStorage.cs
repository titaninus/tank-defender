﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using Utilities;

namespace Tank.Weapon
{
    [CreateAssetMenu(fileName = "WeaponStorage", menuName = "Tools/CreateWeaponStorage", order = 0)]
    public class TankWeaponStorage : ScriptableObject
    {

        [SerializeReference]
        public List<IWeapon> Weapons;

        public int DefaultWeaponIndex = 0;
        private int _currentWeaponIndex = 0;
        
        public IWeapon GetDefaultWeapon()
        {
            _currentWeaponIndex = DefaultWeaponIndex;
            return Weapons[DefaultWeaponIndex];
        }
        
        public IWeapon GetCurrentWeapon()
        {
            return Weapons[_currentWeaponIndex];
        }

        public IWeapon GetNextWeapon()
        {
            _currentWeaponIndex = (_currentWeaponIndex + 1) % Weapons.Count;
            return Weapons[_currentWeaponIndex];
        }

        public IWeapon GetPrevWeapon()
        {
            
            _currentWeaponIndex = (_currentWeaponIndex - 1 + Weapons.Count) % Weapons.Count;
            return Weapons[_currentWeaponIndex];
        }
        
        public IWeapon GetNextWeaponSafe()
        {
            var nextWeaponIndex = (_currentWeaponIndex + 1) % Weapons.Count;
            return Weapons[nextWeaponIndex];
        }

        public IWeapon GetPrevWeaponSafe()
        {
            
            var prevWeaponIndex = (_currentWeaponIndex - 1 + Weapons.Count) % Weapons.Count;
            return Weapons[prevWeaponIndex];
        }


    }
}