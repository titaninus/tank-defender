﻿using System;

namespace Tank.Weapon
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class ExcludeWeaponFromStorageAttribute : System.Attribute
    {
        public ExcludeWeaponFromStorageAttribute()
        {
        }
    }
}