﻿using System;

namespace Tank.Weapon
{
    [AttributeUsage(AttributeTargets.Class)]
    public class WeaponTypeDisplayNameAttribute : System.Attribute
    {
        public readonly string DisplayName;
        public WeaponTypeDisplayNameAttribute(string displayName)
        {
            DisplayName = displayName;
        }
    }
}