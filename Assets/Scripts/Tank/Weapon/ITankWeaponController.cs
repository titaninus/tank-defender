﻿using System.Collections.Generic;
using UnityEngine.Events;

namespace Tank.Weapon
{
    public interface ITankWeaponController
    {
        void PickNextWeapon();
        void PickPrevWeapon();
        void MakeShot();
        List<IWeapon> GetWeapons();
        IWeapon GetCurrentWeapon();
        UnityEvent OnWeaponChanged { get;  }
        TankWeaponStorage GetWeaponStorage();
        float GetCurrentCooldown();
    }
}