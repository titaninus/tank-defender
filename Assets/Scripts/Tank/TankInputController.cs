﻿using System;
using Core;
using Tank.Movement;
using Tank.Weapon;
using UnityEngine;

namespace Tank
{
    public class TankInputController : MonoBehaviour
    {
        private ITankMovementController MoveController;
        private ITankWeaponController WeaponController;
        
        public KeyCode MoveForwardKey = KeyCode.UpArrow;
        public KeyCode MoveBackwardKey = KeyCode.DownArrow;
        public KeyCode RotateLeftKey = KeyCode.LeftArrow;
        public KeyCode RotateRightKey = KeyCode.RightArrow;

        public KeyCode ShotKey = KeyCode.X;
        
        public KeyCode NextWeaponKey = KeyCode.Q;
        public KeyCode PreviousWeaponKey = KeyCode.W;

        private void Start()
        {
            MoveController = DependencyResolver.GetTankMovementController();
            WeaponController = DependencyResolver.GetTankWeaponController();
        }

        private void Update()
        {
            if (!Input.anyKey || !DependencyResolver.GetGameManager().IsGameRunning || DependencyResolver.GetGameManager().IsGamePaused)
            {
                return;
            }
            var isForwardKey = Input.GetKey(MoveForwardKey);
            var isBackwardKey = Input.GetKey(MoveBackwardKey);
            var isLeftKey = Input.GetKey(RotateLeftKey);
            var isRightKey = Input.GetKey(RotateRightKey);
            
            var isShotKey = Input.GetKeyDown(ShotKey);
            var isNextWeaponKey = Input.GetKeyDown(NextWeaponKey);
            var isPreviousWeaponKey = Input.GetKeyDown(PreviousWeaponKey);

            if (isForwardKey)
            {
                if (!isBackwardKey)
                {
                    MoveController.Move(true);
                }
            }

            if (isBackwardKey)
            {
                if (!isForwardKey)
                {
                    MoveController.Move(false);
                }
            }

            if (isLeftKey)
            {
                if (!isRightKey)
                {
                    MoveController.Rotate(true);
                }
            }

            if (isRightKey)
            {
                if (!isLeftKey)
                {
                    MoveController.Rotate(false);
                }
            }

            if (isShotKey)
            {
                WeaponController.MakeShot();
            }

            if (isNextWeaponKey)
            {
                if (!isPreviousWeaponKey)
                {
                    WeaponController.PickNextWeapon();
                }
            }

            if (isPreviousWeaponKey)
            {
                if (!isNextWeaponKey)
                {
                    WeaponController.PickPrevWeapon();
                }
            }
        }
    }
}