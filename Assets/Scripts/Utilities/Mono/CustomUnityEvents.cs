﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Utilities.Mono
{
   [Serializable]
   public class FloatEvent: UnityEvent<float> {}
}