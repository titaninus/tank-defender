﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Utilities
{
    public static class ReflectionUtility
    {
        public static List<Type> GetAllExclusiveInheritorsOf(Type target)
        {
            return AppDomain.CurrentDomain.GetAllTypes()
                .Where(p => target.IsAssignableFrom(p) && p != target ).ToList();
        }
        
        public static List<Type> GetAllExclusiveInheritorsOf(string qualifiedName)
        {
            var target = Type.GetType(qualifiedName);
            if (target == null)
            {
                return new List<Type>();
            }
            return AppDomain.CurrentDomain.GetAllTypes()
                .Where(p => target.IsAssignableFrom(p) && p != target ).ToList();
        }
    }
}