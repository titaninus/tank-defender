﻿using Tank.Weapon;
using UnityEngine.UIElements;

namespace Editor.Inspectors.WeaponStorage.TankWeaponInspectors
{
    public abstract class TankWeaponInspectorBase
    {
        internal abstract VisualElement GetInspector(WeaponStorageInspector.WeaponTypeWithName weaponType, IWeapon targetWeapon);
        internal abstract IWeapon GetRealization();
        internal abstract VisualElement GetRoot();
    }
}