﻿using System;
using System.Linq;
using Utilities;

namespace Editor.Inspectors.WeaponStorage.TankWeaponInspectors
{
    [AttributeUsage(AttributeTargets.Class)]
    public class CustomTankWeaponInspector : System.Attribute
    {
        public readonly Type InspectorType;
        public CustomTankWeaponInspector(Type inspectorType)
        {
            InspectorType = inspectorType;
        }
        
        public CustomTankWeaponInspector(string inspectorType)
        {
            InspectorType = GetInspectorWithName(inspectorType);
        }

        private Type GetInspectorWithName(string name)
        {
            return ReflectionUtility.GetAllExclusiveInheritorsOf("TankWeaponInspectorBase").First(t => t.Name == name);
        }
    }
}