﻿using System;
using System.Reflection;
using Tank.Weapon;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using Utilities;
using Object = UnityEngine.Object;

namespace Editor.Inspectors.WeaponStorage.TankWeaponInspectors
{
    public class TankWeaponReflectionInspector : TankWeaponInspectorBase
    {

        private IWeapon _realization;
        private VisualElement _root;
        private Label _label;
        private WeaponStorageInspector.WeaponTypeWithName _weaponType;

        internal override VisualElement GetInspector(WeaponStorageInspector.WeaponTypeWithName weaponType, IWeapon targetWeapon)
        {
            _realization = targetWeapon;
            _weaponType = weaponType;
            var publicFields = weaponType.WeaponType.GetFields(BindingFlags.Public | BindingFlags.Instance);
            _root = new VisualElement();
            _label = new Label(GetWeaponName());
            _label.style.fontSize = 16;
            _root.Add(_label);
            if (publicFields.Length <= 0)
            {
                throw new ArgumentException($"Couldn't apply TankWeaponReflectionInspector to {weaponType} because it doesn't have any public fields");
            }
            foreach (var publicField in publicFields)
            {
                if (publicField.FieldType.IsSubclassOf(typeof(Object)))
                {

                    var field = new ObjectField(publicField.Name)
                    {
                        objectType = publicField.FieldType
                    };
                    field.RegisterValueChangedCallback(ev => OnValueChanged(publicField, ev.newValue));
                    field.SetValueWithoutNotify(publicField.GetValue(_realization) as Object);
                    _root.Add(field);
                }
                else
                {
                    var field = FieldFactory.CreateField(publicField.FieldType, publicField.GetValue(_realization),
                        o => OnValueChanged(publicField, o), publicField.Name);
                    _root.Add(field);
                }

            }

            return _root;
        }

        private void OnValueChanged(FieldInfo field, object value)
        {
            field.SetValue(_realization, value);
            var name = GetWeaponName();
            _label.text = name;
        }

        private string GetWeaponName()
        {
            var name = _realization.GetName();
            if (string.IsNullOrEmpty(name))
            {
                name = _weaponType.DisplayName;
            }

            return name;
        }
        
        internal override IWeapon GetRealization()
        {
            return _realization;
        }

        internal override VisualElement GetRoot()
        {
            return _root;
        }
    }
}