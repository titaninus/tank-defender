﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Editor.Inspectors.WeaponStorage.TankWeaponInspectors;
using Tank.Weapon;
using UnityEditor;
using UnityEngine.UIElements;
using Utilities;

namespace Editor.Inspectors.WeaponStorage
{
   [CustomEditor(typeof(TankWeaponStorage))]
   public class WeaponStorageInspector : UnityEditor.Editor
   {

      internal class WeaponTypeWithName
      {
         public string DisplayName;
         public Type WeaponType;
      }
   
      private VisualElement _weaponButtonsFoldout;
      private VisualElement _weaponsContainer;

      private List<IWeapon> _weaponsList;
      private Dictionary<IWeapon, TankWeaponInspectorBase> _weaponToInspectorMapping;
      private Dictionary<IWeapon, WeaponTypeWithName> _weaponToTypeMapping;
      private Dictionary<Type, Type> _weaponTypeToInspectorTypeMapping;

      public override VisualElement CreateInspectorGUI()
      {
         InitializeCustomInspectors();
         var root = new VisualElement();
         AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Editor/Inspectors/WeaponStorage/Schemas/WeaponStorageInspector/WeaponStorage.uxml").CloneTree(root);
         //Get containers
         _weaponButtonsFoldout = root.Q("TankWeaponContainer");
         _weaponsContainer = root.Q("WeaponsContainer");
         //Set up properties and actions
         root.Q<Label>("StorageFileName").text = target.name;
         root.Q<Button>("SaveButton").clickable = new Clickable(SaveAllWeapons);
         
         // Find and add buttons for different types of weapons
         foreach (var weaponTypeWithName in GetAllWeaponTypeScripts())
         {
            var button = new Button (() => CreateInstanceOfType(weaponTypeWithName))
            {
               text = weaponTypeWithName.DisplayName
            };
            _weaponButtonsFoldout.Add(button);
         }
         _weaponToInspectorMapping = new Dictionary<IWeapon, TankWeaponInspectorBase>();
         _weaponToTypeMapping = new Dictionary<IWeapon, WeaponTypeWithName>();
         _weaponsList = (target as TankWeaponStorage).Weapons;
         if (_weaponsList != null)
         {
            // Simply break connection between original weapon list and list controlled by this container (ToList make an copy of list)
            _weaponsList = _weaponsList.ToList();
            var ignoreWeapons = new List<IWeapon>();
            foreach (var weapon in _weaponsList)
            {
               var weaponWithName = GetTypeWithNameFor(weapon.GetType());
               if (weaponWithName != null)
               {
                  _weaponToTypeMapping.Add(weapon, weaponWithName);
               }
               else
               {
                  ignoreWeapons.Add(weapon);
               }
            }

            foreach (var ignoreWeapon in ignoreWeapons)
            {
               _weaponsList.Remove(ignoreWeapon);
            }
         }
         else
         {
            _weaponsList = new List<IWeapon>();
         }
         UpdateWeapons();
         return root;
      }

      private void UpdateWeapons()
      {
         foreach (var weapon in _weaponsList)
         {
            if (!_weaponToInspectorMapping.ContainsKey(weapon))
            {
               var inspector = GetSuitableInspectorFor(_weaponToTypeMapping[weapon].WeaponType);
               _weaponToInspectorMapping.Add(weapon, inspector);
               _weaponsContainer.Add(inspector.GetInspector(_weaponToTypeMapping[weapon], weapon));
            }
         }
      }
      
      private void SaveAllWeapons()
      {
         CheckForImages();
         (target as TankWeaponStorage).Weapons = _weaponsList;
         EditorUtility.SetDirty(target);
         AssetDatabase.SaveAssets();
      }

      private void CheckForImages()
      {
         foreach (var weapon in _weaponsList)
         {
            if (weapon.GetWeaponImage() == null)
            {
               weapon.SetWeaponImage(AssetPreview.GetAssetPreview(weapon.GetWeaponPrefab()));
            }
         }
      }

      private void DeleteWeapon(IWeapon targetWeapon)
      {
         _weaponsList.Remove(targetWeapon);
         _weaponToTypeMapping.Remove(targetWeapon);
         var inspector = _weaponToInspectorMapping[targetWeapon];
         _weaponsContainer.Remove(inspector.GetRoot());
         _weaponToInspectorMapping.Remove(targetWeapon);
         UpdateWeapons();
      }
      
      private void CreateInstanceOfType(WeaponTypeWithName weaponType)
      {
         var targetWeapon = Activator.CreateInstance(weaponType.WeaponType) as IWeapon;
         if (targetWeapon == null)
         {
            throw new ArgumentException(
               $"Invalid weaponType {weaponType.DisplayName} - should be realization of interface IWeapon");
         }

         _weaponsList.Add(targetWeapon);
         _weaponToTypeMapping.Add(targetWeapon, weaponType);
         UpdateWeapons();
      }

      private void InitializeCustomInspectors()
      {
         _weaponTypeToInspectorTypeMapping = new Dictionary<Type, Type>();
         var inspectors = ReflectionUtility.GetAllExclusiveInheritorsOf(typeof(TankWeaponInspectorBase));
         foreach (var inspector in inspectors)
         {
            var attributes = inspector.GetCustomAttributes<CustomTankWeaponInspector>();
            foreach (var customMonsterInspector in attributes)
            {
               _weaponTypeToInspectorTypeMapping.Add(customMonsterInspector.InspectorType, inspector);
            }
         }
      }
      
      private TankWeaponInspectorBase GetSuitableInspectorFor(Type weaponType)
      {
         if (_weaponTypeToInspectorTypeMapping.ContainsKey(weaponType))
         {
            return Activator.CreateInstance(_weaponTypeToInspectorTypeMapping[weaponType]) as TankWeaponInspectorBase;
         }
         
         return Activator.CreateInstance<TankWeaponReflectionInspector>();
         
      }
      
      private static List<WeaponTypeWithName> GetAllWeaponTypeScripts()
      {
         var weapons = ReflectionUtility.GetAllExclusiveInheritorsOf(typeof(IWeapon));
         var results = new List<WeaponTypeWithName>();
         foreach (var weapon in weapons)
         {
            // Check if we exclude this realization from weapon storage
            var weaponWithName = GetTypeWithNameFor(weapon);
            if (weaponWithName != null)
            {
               results.Add(weaponWithName);
            }
         }

         return results;
      }

      private static WeaponTypeWithName GetTypeWithNameFor(Type targetType)
      {
         var excludeAttribute = targetType.GetCustomAttribute(typeof(ExcludeWeaponFromStorageAttribute)) as ExcludeWeaponFromStorageAttribute;
         if (excludeAttribute != null)
         {
            return null;
         }

         var displayName = targetType.Name;
         
         // Check if we specify display name
         var nameAttribute = targetType.GetCustomAttribute(typeof(WeaponTypeDisplayNameAttribute)) as WeaponTypeDisplayNameAttribute;
         if (nameAttribute != null)
         {
            displayName = nameAttribute.DisplayName;
         }
         
         return new WeaponTypeWithName()
         {
            DisplayName = displayName,
            WeaponType = targetType
         };
      }
      
   }
   
}
