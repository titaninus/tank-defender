﻿using System;
using System.Reflection;
using Monster;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using Utilities;
using Object = UnityEngine.Object;

namespace Editor.Inspectors.MonsterStorage.MonsterInspectors
{
    public class MonsterReflectionInspector : MonsterInspectorBase
    {

        private IMonster _realization;
        private VisualElement _root;
        private Label _label;
        private MonsterStorageInspector.MonsterTypeWithName _monsterType;
        private Action<IMonster> _deleteAction;

        internal override VisualElement GetInspector(MonsterStorageInspector.MonsterTypeWithName monsterType, IMonster targetMonster, Action<IMonster> deleteAction)
        {
            _deleteAction = deleteAction;
            _realization = targetMonster;
            _monsterType = monsterType;
            var publicFields = monsterType.MonsterType.GetFields(BindingFlags.Public | BindingFlags.Instance);
            _root = new VisualElement();
            _label = new Label(GetMonsterName());
            _label.style.fontSize = 16;
            _root.Add(_label);
            var deleteButton = new Button(OnDeleteButtonClicked)
            {
                text = "Delete monster"
            };
            _root.Add(deleteButton);
            if (publicFields.Length <= 0)
            {
                throw new ArgumentException($"Couldn't apply MonsterReflectionInspector to {monsterType} because it doesn't have any public fields");
            }
            foreach (var publicField in publicFields)
            {
                if (publicField.FieldType.IsSubclassOf(typeof(Object)))
                {

                    var field = new ObjectField(publicField.Name)
                    {
                        objectType = publicField.FieldType
                    };
                    field.RegisterValueChangedCallback(ev => OnValueChanged(publicField, ev.newValue));
                    field.SetValueWithoutNotify(publicField.GetValue(_realization) as Object);
                    _root.Add(field);
                }
                else
                {
                    var field = FieldFactory.CreateField(publicField.FieldType, publicField.GetValue(_realization),
                        o => OnValueChanged(publicField, o), publicField.Name);
                    _root.Add(field);
                }

            }

            return _root;
        }

        private void OnDeleteButtonClicked()
        {
            _deleteAction(_realization);
        }

        private void OnValueChanged(FieldInfo field, object value)
        {
            field.SetValue(_realization, value);
            var name = GetMonsterName();
            _label.text = name;
        }

        private string GetMonsterName()
        {
            var name = _realization.GetName();
            if (string.IsNullOrEmpty(name))
            {
                name = _monsterType.DisplayName;
            }

            return name;
        }
        
        internal override IMonster GetRealization()
        {
            return _realization;
        }

        internal override VisualElement GetRoot()
        {
            return _root;
        }
    }
}