﻿using System;
using Monster;
using UnityEngine.UIElements;

namespace Editor.Inspectors.MonsterStorage.MonsterInspectors
{
    public abstract class MonsterInspectorBase
    {
        internal abstract VisualElement GetInspector(MonsterStorageInspector.MonsterTypeWithName monsterType, IMonster targetMonster, Action<IMonster> deleteAction);
        internal abstract IMonster GetRealization();
        internal abstract VisualElement GetRoot();
    }
}