﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Editor.Inspectors.MonsterStorage.MonsterInspectors;
using Monster;
using UnityEditor;
using UnityEngine.UIElements;
using Utilities;

namespace Editor.Inspectors.MonsterStorage
{
    [CustomEditor(typeof(Monster.MonsterStorage))]
    public class MonsterStorageInspector : UnityEditor.Editor
    {
        internal class MonsterTypeWithName
        {
            public string DisplayName;
            public Type MonsterType;
        }

        private VisualElement _monsterButtonsFoldout;
        private VisualElement _monstersContainer;

        private List<IMonster> _monstersList;
        private Dictionary<IMonster, MonsterInspectorBase> _monsterToInspectorMapping;
        private Dictionary<IMonster, MonsterTypeWithName> _monsterToTypeMapping;
        private Dictionary<Type, Type> _monsterTypeToInspectorTypeMapping;

        public override VisualElement CreateInspectorGUI()
        {
            InitializeCustomInspectors();
            var root = new VisualElement();
            AssetDatabase
                .LoadAssetAtPath<VisualTreeAsset>(
                    "Assets/Editor/Inspectors/MonsterStorage/Schemas/MonsterStorageInspector/MonsterStorage.uxml")
                .CloneTree(root);
            //Get containers
            _monsterButtonsFoldout = root.Q("MonsterButtonContainer");
            _monstersContainer = root.Q("MonsterContainer");
            //Set up properties and actions
            root.Q<Label>("StorageFileName").text = target.name;
            root.Q<Button>("SaveButton").clickable = new Clickable(SaveAllMonsters);

            // Find and add buttons for different types of weapons
            foreach (var monsterTypeWithName in GetAllMonsterTypeScripts())
            {
                var button = new Button(() => CreateInstanceOfType(monsterTypeWithName))
                {
                    text = monsterTypeWithName.DisplayName
                };
                _monsterButtonsFoldout.Add(button);
            }

            _monsterToInspectorMapping = new Dictionary<IMonster, MonsterInspectorBase>();
            _monsterToTypeMapping = new Dictionary<IMonster, MonsterTypeWithName>();
            _monstersList = (target as Monster.MonsterStorage).Monsters;
            if (_monstersList != null)
            {
                // Simply break connection between original weapon list and list controlled by this container (ToList make an copy of list)
                _monstersList = _monstersList.ToList();
                var ignoreMonsters = new List<IMonster>();
                foreach (var monster in _monstersList)
                {
                    var monsterWithName = GetTypeWithNameFor(monster.GetType());
                    if (monsterWithName != null)
                    {
                        _monsterToTypeMapping.Add(monster, monsterWithName);
                    }
                    else
                    {
                        ignoreMonsters.Add(monster);
                    }
                }

                foreach (var monster in ignoreMonsters)
                {
                    _monstersList.Remove(monster);
                }
            }
            else
            {
                _monstersList = new List<IMonster>();
            }

            UpdateMonsters();
            return root;
        }

        private void UpdateMonsters()
        {
            foreach (var monster in _monstersList)
            {
                if (!_monsterToInspectorMapping.ContainsKey(monster))
                {
                    var inspector = GetSuitableInspectorFor(_monsterToTypeMapping[monster].MonsterType);
                    _monsterToInspectorMapping.Add(monster, inspector);
                    var inspectorElement = inspector.GetInspector(_monsterToTypeMapping[monster], monster, DeleteMonster);
                    _monstersContainer.Add(inspectorElement);
                }
            }
        }

        private void SaveAllMonsters()
        {
            (target as Monster.MonsterStorage).Monsters = _monstersList;
            EditorUtility.SetDirty(target);
            AssetDatabase.SaveAssets();
        }

        private void DeleteMonster(IMonster targetMonster)
        {
            _monstersList.Remove(targetMonster);
            _monsterToTypeMapping.Remove(targetMonster);
            var inspector = _monsterToInspectorMapping[targetMonster];
            _monstersContainer.Remove(inspector.GetRoot());
            _monsterToInspectorMapping.Remove(targetMonster);
            UpdateMonsters();
        }

        private void CreateInstanceOfType(MonsterTypeWithName monsterType)
        {
            var targetMonster = Activator.CreateInstance(monsterType.MonsterType) as IMonster;
            if (targetMonster == null)
            {
                throw new ArgumentException(
                    $"Invalid monsterType {monsterType.DisplayName} - should be realization of interface IMonster");
            }

            _monstersList.Add(targetMonster);
            _monsterToTypeMapping.Add(targetMonster, monsterType);
            UpdateMonsters();
        }

        private void InitializeCustomInspectors()
        {
            _monsterTypeToInspectorTypeMapping = new Dictionary<Type, Type>();
            var inspectors = ReflectionUtility.GetAllExclusiveInheritorsOf(typeof(MonsterInspectorBase));
            foreach (var inspector in inspectors)
            {
                var attributes = inspector.GetCustomAttributes<CustomMonsterInspector>();
                foreach (var customMonsterInspector in attributes)
                {
                    _monsterTypeToInspectorTypeMapping.Add(customMonsterInspector.InspectorType, inspector);
                }
            }
        }
        
        private MonsterInspectorBase GetSuitableInspectorFor(Type monsterType)
        {
            if (_monsterTypeToInspectorTypeMapping.ContainsKey(monsterType))
            {
                return Activator.CreateInstance(_monsterTypeToInspectorTypeMapping[monsterType]) as MonsterInspectorBase;
            }
             
            return Activator.CreateInstance<MonsterReflectionInspector>();
        }

        private static List<MonsterTypeWithName> GetAllMonsterTypeScripts()
        {
            var monsters = ReflectionUtility.GetAllExclusiveInheritorsOf(typeof(IMonster));
            var results = new List<MonsterTypeWithName>();
            foreach (var monster in monsters)
            {
                // Check if we exclude this realization from monster storage
                var monsterWithName = GetTypeWithNameFor(monster);
                if (monsterWithName != null)
                {
                    results.Add(monsterWithName);
                }
            }

            return results;
        }

        private static MonsterTypeWithName GetTypeWithNameFor(Type targetType)
        {
            var excludeAttribute =
                targetType.GetCustomAttribute(typeof(ExcludeMonsterFromStorageAttribute)) as
                    ExcludeMonsterFromStorageAttribute;
            if (excludeAttribute != null)
            {
                return null;
            }

            var displayName = targetType.Name;

            // Check if we specify display name
            var nameAttribute =
                targetType.GetCustomAttribute(typeof(MonsterTypeDisplayNameAttribute)) as
                    MonsterTypeDisplayNameAttribute;
            if (nameAttribute != null)
            {
                displayName = nameAttribute.DisplayName;
            }

            return new MonsterTypeWithName()
            {
                DisplayName = displayName,
                MonsterType = targetType
            };
        }
    }
}