﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Monster;
using Monster.MonsterBehaviour;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using Utilities;

namespace Editor.Inspectors.MonsterStorage.Fields
{
    [FieldDrawer(typeof(MonsterBehaviourType))]
    public class MonsterBehaviourTypeField : VisualElement, INotifyValueChanged<MonsterBehaviourType>
    {
        private const string NonSelectedPopupName = "Non selected";
        
        private PopupField<string> _popup;
        private MonsterBehaviourType _value;
        private List<string> _choices;

        public MonsterBehaviourTypeField() : this(null)
        {
        }
        
        public MonsterBehaviourTypeField(string label)
        {
            _choices = new List<string>()
            {
                NonSelectedPopupName
            }; 
            _choices.AddRange(CollectMonsterBehaviourRealizations());
            _popup = new PopupField<string>(label, _choices, 0);
            _popup.RegisterValueChangedCallback(OnPopupValueChanged);
            Add(_popup);
        }

        private List<string> CollectMonsterBehaviourRealizations()
        {
            return ReflectionUtility
                .GetAllExclusiveInheritorsOf(typeof(MonsterBehaviourBase))
                .Where(t => !t.GetCustomAttributes<ExcludeMonsterBehaviourTypeFromStorageAttribute>().Any())
                .Select(t => t.AssemblyQualifiedName)
                .ToList();
        }

        private void OnPopupValueChanged(ChangeEvent<string> evt)
        {
            MonsterBehaviourType newValue = null;
            var prevValue = value;
            if (!string.IsNullOrEmpty(evt.newValue) && evt.newValue != NonSelectedPopupName)
            {
                var type = Type.GetType(evt.newValue);
                if (type != null && type.IsSubclassOf(typeof(MonsterBehaviourBase)))
                {
                    var monsterBehaviour = new MonsterBehaviourType()
                    {
                        QualifiedMonsterBehaviourType = evt.newValue
                    };
                    newValue = monsterBehaviour;
                    
                }
            }
            using (ChangeEvent<MonsterBehaviourType> pooled =
                ChangeEvent<MonsterBehaviourType>.GetPooled(prevValue, newValue))
            {
                pooled.target = (IEventHandler) this;
                this.SetValueWithoutNotify(newValue);
                this.SendEvent((EventBase) pooled);
            }
        }

        public void SetValueWithoutNotify(MonsterBehaviourType newValue)
        {
            _value = newValue;
            if (_value != null)
            {
                var index = _choices.IndexOf(_value.QualifiedMonsterBehaviourType);
                index = Math.Max(0, index);
                _popup.index = index;
            }
            else
            {
                _popup.index = 0;
            }
        }

        public MonsterBehaviourType value
        {
            get => _value;
            set => SetValueWithoutNotify(value);
        }
    }
}