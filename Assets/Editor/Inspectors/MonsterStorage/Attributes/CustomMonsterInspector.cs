﻿using System;
using System.Linq;
using Utilities;

namespace Monster
{
    [AttributeUsage(AttributeTargets.Class)]
    public class CustomMonsterInspector : System.Attribute
    {
        public readonly Type InspectorType;
        public CustomMonsterInspector(Type monsterType)
        {
            InspectorType = monsterType;
        }
    }
}