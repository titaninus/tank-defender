﻿using Monster;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor.Inspectors.Fields
{
    public class PrefabPreviewField : VisualElement, INotifyValueChanged<GameObject>
    {
        private ObjectField _prefabField;
        private Image _assetPreview;
        private Label _label;
        private VisualElement _container;
        private GameObject _value;

        public PrefabPreviewField() : this(null)
        {
        }
        
        public PrefabPreviewField(string label)
        {
            _label = new Label(label);
            _container = new VisualElement();
            _container.style.flexDirection = FlexDirection.Row;
            _assetPreview = new Image();
            _assetPreview.style.flexBasis = new StyleLength(new Length(40f, LengthUnit.Percent));
            _assetPreview.style.minHeight = 64;
            _assetPreview.style.minWidth = 64;
            _assetPreview.scaleMode = ScaleMode.ScaleToFit;
            _prefabField = new ObjectField
            {
                objectType = typeof(GameObject), 
                allowSceneObjects = false
            };
            _prefabField.style.flexBasis = new StyleLength(new Length(60f, LengthUnit.Percent));
            _prefabField.style.maxHeight = 20;
            _prefabField.RegisterValueChangedCallback(OnPrefabChanged);
            _container.Add(_assetPreview);
            _container.Add(_prefabField);
            Add(_label);
            Add(_container);
        }

        private void OnPrefabChanged(ChangeEvent<Object> evt)
        {
            var newValue = evt.newValue as GameObject;
            using (ChangeEvent<GameObject> pooled =
                ChangeEvent<GameObject>.GetPooled(_value, newValue))
            {
                pooled.target = (IEventHandler) this;
                this.SetValueWithoutNotify(newValue);
                this.SendEvent((EventBase) pooled);
            }
        }

        public void SetValueWithoutNotify(GameObject newValue)
        {
            _value = newValue;
            _prefabField.SetValueWithoutNotify(_value);
            if (_value != null)
            {
                _assetPreview.image = AssetPreview.GetAssetPreview(_value);
            }
        }

        public GameObject value
        {
            get => _value;
            set => SetValueWithoutNotify(_value);
        }
    }
}