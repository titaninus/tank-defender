﻿using Core;
using UnityEditor;
using UnityEngine;

namespace Editor.Inspectors.Core
{
    [CustomEditor(typeof(GameManager))]
    public class GameManagerCustomEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            
            base.OnInspectorGUI();
            if (GUILayout.Button("RunGame"))
            {
                (target as GameManager).RunGame();
            }
        }
    }
}