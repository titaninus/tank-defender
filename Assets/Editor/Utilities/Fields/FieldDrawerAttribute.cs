﻿using System;

namespace Utilities
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class FieldDrawerAttribute : Attribute
    {
        public Type		fieldType;

        public FieldDrawerAttribute(Type fieldType)
        {
            this.fieldType = fieldType;
        }
    }
}